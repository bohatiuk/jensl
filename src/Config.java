@Singleton(strict = false)
class Configuration implements Serializable {
    static def config
    
    Configuration(customConfig) {

        config = [
            'TIMEOUT': customConfig?.TIMEOUT ?: 60
        ]
    }
}
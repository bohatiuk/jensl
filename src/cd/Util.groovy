package cd


import java.text.DateFormat

import java.text.SimpleDateFormat

import com.cloudbees.groovy.cps.NonCPS


class Util implements Serializable {


  def conf

  def script

  def currentBuild


  def Util(script, conf, currentBuild) { //def here is reduntant

    this.script = script

    this.conf = conf

    this.currentBuild = currentBuild

  }



  def doGoal (GOAL, LOG, PARAMETERS = '') {

    if (conf.ADMIN_COMMANDS[conf.BUILDTOOL] == null) {

      script.error "Unknown buildtool"

    } else if (conf.ADMIN_COMMANDS[conf.BUILDTOOL][GOAL] == null) {

      script.error "Invalid action for ${conf.BUILDTOOL} build tool and goal ${GOAL}"

    } else if (conf.ADMIN_COMMANDS[conf.BUILDTOOL][GOAL] == '') {

      script.echo "No command defined for ${conf.BUILDTOOL} build tool and goal ${GOAL}"

      return

    }

    switch (conf.BUILDTOOL) {

      case 'maven':

        doMaven("${conf.ADMIN_COMMANDS[conf.BUILDTOOL][GOAL]} ${DEPSENFORCE}${PARAMETERS}", LOG)

        break

      case 'make':      

        doMake("${conf.ADMIN_COMMANDS[conf.BUILDTOOL][GOAL]} ${PARAMETERS}", LOG)

        break

      case 'gradle':

        doGradle(conf.ADMIN_COMMANDS[conf.BUILDTOOL][GOAL], LOG)

        break

      default:

        script.error 'Unknown BUILDTOOL'

        break

    }

  }

}

// LinkedIn coloring snipplets is funky (works unexpectedly)
// the next line is included just for this coloring to work
//class doGoalTest extends GroovyTestCase{
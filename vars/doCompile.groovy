/**Performs compile actions, depending on BUILDTOOL defined

 *

 * @param conf configuration Map.

 * @param agent Jenkins agent label to run this step actions. OPTIONAL, defaults to conf[AGENT].

 */

def call(conf = [:], agent = null) {

  if (conf.conf != null) { //multiple named parameters passed

    agent = conf.agent

    conf = conf.conf

  }


  def util = new cd.Util(this, conf, currentBuild)}


  def handledParameters = util.handleNamedClosureParameters(method: 'Compile', customAgent: agent)

  agent = handledParameters['agent']

  def customworkspace = handledParameters['workspace']

  def parameters = handledParameters['parameters']



  stage ("Compile") {



    node(agent) {

      dir(customworkspace) {

        try {

          util.doGoal('compile', 'COMPILE')

        } catch (berr) {

          conf.INTERNAL_ERRMSG = "Exception on building"

          archive 'COMPILE.log, **/npm-debug.log, **/yarn-error.log'

          conf.INTERNAL_ERRORLOGS.add ('COMPILE')

          throw new GroovyRuntimeException("Exception on building", berr)

        }

      }

    }

  }

}
def call(Map config=[:], Closure body=null) {
    node {
        git url: "https://github.com/werne2j/sample-nodejs"
        stage("Install") {
            sh "echo npm install"
        }
        stage("Test") {
            sh "echo npm test"
        }
        stage("Deploy") {
            if (config.deploy) {
                sh "echo npm publish"
            }
        }
        if (body) {
            body()
        }
        
    }
}
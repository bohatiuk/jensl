package cd.pipelines


import org.junit.Test


class doGoalTest extends GroovyTestCase {

  void echo(object) {

    println "${object}"

  }

  void error(String message) {

    throw new GroovyRuntimeException (message)

  }


  @Test
  void testInvalidTool() {


    def conf = [

        'BUILDTOOL': 'non-maven',

        'ADMIN_COMMANDS' : ['maven': [

            'compile': 'test-compile -DforceAggregate',

        ]

        ]

    ]

    def util = new cd.Util(this, conf, null)

    def msg = shouldFail(GroovyRuntimeException) {

      util.doGoal('goal', 'log', 'params')

    }

    assert 'Unknown buildtool' == msg

  }



  @Test
  void testInvalidMavenGoal() {


    def conf = [

        'BUILDTOOL': 'maven',

        'ADMIN_COMMANDS' : ['maven': [

            'compile': 'test-compile -DforceAggregate',

            ]

          ]

        ]

    def util = new cd.Util(this, conf, null)


    def msg = shouldFail(GroovyRuntimeException) {

      util.doGoal('non-compile', '', '')

    }

    assert 'Invalid action for maven build tool and goal non-compile' == msg

  }
}